import ModeloUsuario from "../models/modeloUsuario.js";
import  jwt  from "jsonwebtoken";
import bcryptjs from 'bcryptjs';
import {promisify} from 'util';



//Mostrar todos los usuarios

export const getTodosLosUsuarios = async (req, res) => {
    try {
        const usuarios = await ModeloUsuario.findAll()
        res.json(usuarios)
    } catch (error) {
        res.json( {message: error.message} )
    }
}


//Registrar usuario

export const createUsuario = async (req, res) => {
    try {
       const pass = req.body.Password;
       let passHash = await bcryptjs.hash(pass, 8);
       await ModeloUsuario.create({Usuario: req.body.Usuario, Nombre:req.body.Nombre, Password:passHash});  
       res.json({
        Usuario: req.body.Usuario, Nombre:req.body.Nombre, Password:passHash
       })
    } catch (error) {
        res.json( {message: error.message} );
    }
}


//Login usuario

let flagAlert;

const comparacionContraseñas = async (pass,passHash) =>{
    return await bcryptjs.compare(pass, passHash)
}

export const loginUsuario = async (req, res)=>{
 
   
  
     try {
        const { Usuario, Password } = req.body;
        const pass = Password;
        if(!Usuario || !Password ){
            res.json( {error: "campos vacios"} )
        }
        else{     
            const user = await ModeloUsuario.findOne({ where: { Usuario: Usuario } });

            if (!user){ 
                res.json({ error: "Usuario no existe" });
            }    
            else{
                const comparacion= await comparacionContraseñas(pass, user.Password)
                if (!comparacion){ 
                    res.json({ error: 'contraseña incorrecta'});
                }
                else{
                    const token = jwt.sign(
                        { Usuario: user.Usuario, id: user.id },
                        process.env.JWT_SECRETO);
                    
                    const cookiesOptions = {
                        expires: new Date(Date.now()+process.env.JWT_COOKIE_EXPIRES * 24 * 60 * 60 * 1000),
                        httpOnly: false,
                        secure: true
                    }
                    
                    res.cookie('jwt', token, cookiesOptions)
                    res.json( {error: ''} )
                    
                }
            }
        }    
    }
    catch (error) {
        console.log(error)
    }
}
                  
      /*  }else{
            conexion.query('SELECT * FROM usuarios WHERE user = ?', [user], async (error, results)=>{
                if( results.length == 0 || ! (await bcryptjs.compare(pass, results[0].pass)) ){
                    res.json( {message: 'usuario o contraseña incorrectas'} )
                }else{
                    //inicio de sesión OK
                    const id = results[0].id
                    const token = jwt.sign({id:id}, process.env.JWT_SECRETO, {
                        expiresIn: process.env.JWT_TIEMPO_EXPIRA
                    })
                    //generamos el token SIN fecha de expiracion
                   //const token = jwt.sign({id: id}, process.env.JWT_SECRETO)
                   console.log("TOKEN: "+token+" para el USUARIO : "+user)

                   const cookiesOptions = {
                        expires: new Date(Date.now()+process.env.JWT_COOKIE_EXPIRES * 24 * 60 * 60 * 1000),
                        httpOnly: true
                   }
                   res.cookie('jwt', token, cookiesOptions)
                   res.json( {message: 'login exitoso'} )
                   
                }
            })
        }
    } catch (error) {
        console.log(error)
    }
}
*/

//comprobar si el usuario esta autenticado/con sesion abierta
let usuario
export const siEstaAutenticado = async (req, res, next)=>{
    if (req.cookies.jwt){//console.log(req.cookies.jwt) console.log(decodificada)
        try {
            
            const decodificada = await promisify(jwt.verify)(req.cookies.jwt, process.env.JWT_SECRETO)
             usuario = await ModeloUsuario.findOne({ where: { id: decodificada.id } });  
            console.log(usuario.id)
            if(!usuario){
                    return next()
                }
                req.Usuario = usuario
                res.json( {message: 'si esta autenticado'} )
                return next()
                
        } catch (error) {
            console.log(error)
            return next()
        }
    }else{
        //res.redirect('/login')
        res.json( {message: ''} )// lo que voy a hacer es mandar esta funcion con una peticion get que se ejecute con un useeffect en la pantalla de login y que segun el json que me ponga redirija
    }
}


//logout

export const logout = (req, res)=>{
    res.clearCookie('jwt')   
   // return res.redirect('/')
    res.json( {message: 'logout ok'} )
}

//peticion get para el tipo de alert que se debe mostrar
export const tipoDeAlert = async (req, res) => {
    try {
        res.json(flagAlert)
    } catch (error) {
        res.json( {message: error.message} )
    }
}

export const proteccionVistasLogin = async (req, res) => {
    try {
        if (req.cookies.jwt){
            res.json({message:'esta'})
        }
        else{
            res.json({message:'no esta'})
        }
    } catch (error) {
        res.json( {message: error.message} )
    }
}

export const idUsuario = async (req, res) =>{ 
   
    return usuario.id
}

export const nombreUsuario = async (req, res) =>{ 
   
    return res.json(usuario.Usuario)
}