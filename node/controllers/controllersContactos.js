import ModeloContacto from "../models/modeloConctacto.js";
import { idUsuario } from '../controllers/controllersUsuarios.js'

//// METODOS CONTACTOS ////


// Muestra todos los contactos segun el usuario que esta activo
export const getTodosLosContactos = async (req, res) => {
    try {
        let ida = await idUsuario()
        const contactos = await ModeloContacto.findAll({where:{ usuario_id: ida }})
        res.json(contactos)
    } catch (error) {
        res.json( {message: error.message} )
    }
}


// Filtra los contactos genero hombre segun el usuario que esta logeado
export const filtrarHombres = async (req, res) => {
    try {
        let ida = await idUsuario()
        const contactos = await ModeloContacto.findAll({where:{ usuario_id: ida, Sexo:'Hombre' }})
        res.json(contactos)
    } catch (error) {
        res.json( {message: error.message} )
    }
}


// Filtra los contactos genero mujer segun el usuario que esta logeado
export const filtrarMujeres = async (req, res) => {
    try {
        let ida = await idUsuario()
        const contactos = await ModeloContacto.findAll({where:{ usuario_id: ida, Sexo:'Mujer' }})
        res.json(contactos)
    } catch (error) {
        res.json( {message: error.message} )
    }
}


// Busca el contacto que escribio el usuario logeado. Ademas realiza la validacion del nombre enviado desde el front
export const buscadorContactos = async (req, res) => {
    try {
        let ida = await idUsuario()
        const nombreContacto = req.body.Nombre;
        if(!nombreContacto){
            res.json( {error: "campo vacio"} )
        }
        else{     
            const contactos = await ModeloContacto.findAll({where:{ usuario_id: ida, Nombre: req.body.Nombre }})
            console.log(contactos)
            if (!contactos[0]){ 
                res.json({ error: "contacto no existe" });
            }
            else{
                res.json(contactos)
            }
        }      
    } catch (error) {
        res.json( {message: error.message} )
    }
}


// Ordena los contactos por edad con el metodo sort segun el usuario logeado y los envia al front 
export const ordenarPorEdad = async (req, res) => {
    try {
        let ida = await idUsuario()
        const contactos = await ModeloContacto.findAll({where:{ usuario_id: ida }})
        
        contactos.sort((a,b) =>{
            return a.Edad - b.Edad;
        })

        res.json(contactos)
    } catch (error) {
        res.json( {message: error.message} )
    }
}


// Muestra el contacto al que se le hizo click
export const getContacto = async (req, res) => {
    try {
        const contacto = await ModeloContacto.findAll({
            where:{ id:req.params.id }
        })
        res.json(contacto[0])
    } catch (error) {
        res.json( {message: error.message} )
    }
}


//vCrear un contacto que se mostrara segun la sesion del usuario que este activa
export const createContacto = async (req, res) => {
    
    try {
        let id = await idUsuario()
        await ModeloContacto.create({Nombre:req.body.Nombre, Numero:req.body.Numero, Sexo:req.body.Sexo, Edad:req.body.Edad, usuario_id: id})
       res.json({Nombre:req.body.Nombre, Numero:req.body.Numero, Sexo:req.body.Sexo, Edad:req.body.Edad
})
    } catch (error) {
        res.json( {message: error.message} )
    }
}



//Modificar un contacto

export const updateContacto = async (req, res) => {
    try {
        await ModeloContacto.update(req.body, {
            where: { id: req.params.id}
        })
        res.json({
            Nombre:req.body.Nombre, Numero:req.body.Numero, Sexo:req.body.Sexo, Edad:req.body.Edad})
    } catch (error) {
        res.json( {message: error.message} )
    }
}



//Eliminar un Contacto

export const deleteContacto = async (req, res) => {
    try {
        await ModeloContacto.destroy({ 
            where: { id : req.params.id }
        })
        res.json({
            "message":"Contacto eliminado correctamente"
        })
    } catch (error) {
        res.json( {message: error.message} )
    }
}

/*
export const getTodosLosContactos = async (req, res) => {
    try {
        let ida = await idUsuario()
        const contactos = await ModeloContacto.findAll({where:{ usuario_id: ida }})
        res.json(contactos)
    } catch (error) {
        res.json( {message: error.message} )
    }
}

*/