import express from 'express'
import { buscadorContactos, filtrarHombres, filtrarMujeres, getTodosLosContactos, ordenarPorEdad, getContacto, createContacto, updateContacto, deleteContacto,  } from '../controllers/controllersContactos.js'

export const routerBuscarContacto = express.Router()
export const routerContactos = express.Router()
export const routerOrdenarPorEdad = express.Router()
export const routerFiltrarHombres = express.Router()
export const routerFiltrarMujeres = express.Router()

routerBuscarContacto.post('/', buscadorContactos)
routerOrdenarPorEdad.get('/', ordenarPorEdad)
routerFiltrarHombres.get('/', filtrarHombres)
routerFiltrarMujeres.get('/', filtrarMujeres)

routerContactos.get('/', getTodosLosContactos)
routerContactos.get('/:id', getContacto)
routerContactos.post('/', createContacto)
routerContactos.put('/:id', updateContacto)
routerContactos.delete('/:id', deleteContacto)

