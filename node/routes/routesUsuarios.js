import express from 'express'
import { nombreUsuario, proteccionVistasLogin, getTodosLosUsuarios, createUsuario, loginUsuario, tipoDeAlert, logout, siEstaAutenticado } from '../controllers/controllersUsuarios.js'

export const routerNombreUsuario = express.Router()
export const routerProteccionVistasLogin = express.Router()
export const routerRegistrarUsuario = express.Router()
export const routerLoginUsuario = express.Router()
export const routerTipoDeAlert = express.Router()
export const routerSiEstaAutenticado = express.Router()
export const routerLogout = express.Router()
export const routerGetTodos = express.Router()

routerNombreUsuario.get('/', nombreUsuario)
routerProteccionVistasLogin.get('/', proteccionVistasLogin)
routerGetTodos.get('/', getTodosLosUsuarios)
routerRegistrarUsuario.post('/', createUsuario)
routerLoginUsuario.post('/', loginUsuario)
routerTipoDeAlert.get('/', tipoDeAlert)
routerSiEstaAutenticado.get('/', siEstaAutenticado)
routerLogout.get('/', logout)

