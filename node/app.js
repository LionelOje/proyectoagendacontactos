import express  from "express"
import cors from 'cors'
import dotenv from 'dotenv'
import cookieParser from 'cookie-parser'
//importamos la conexión a la DB
import db from "./dtabase/conexionDtabase.js"
//importamos nuestros enrutadores
import {routerContactos, routerBuscarContacto, routerOrdenarPorEdad, routerFiltrarHombres, routerFiltrarMujeres} from './routes/routesContactos.js'
import {routerNombreUsuario, routerProteccionVistasLogin ,routerGetTodos, routerLoginUsuario, routerTipoDeAlert, routerRegistrarUsuario, routerSiEstaAutenticado, routerLogout} from './routes/routesUsuarios.js'


const app = express()

app.use(cors({credentials: true, origin: 'http://localhost:3000'}));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept"
  );
  if ("OPTIONS" == req.method) {
    res.send(200);
  } else {
    next();
  }
});
//para procesar datos enviados desde forms
app.use(express.urlencoded({extended:true}))
app.use(express.json())

//seteamos las variables de entorno
dotenv.config({path: './variablesEntorno/.env'})

//para poder trabajar con las cookies
app.use(cookieParser())

//ruta para servicios de los contactos
app.use('/contactos', routerContactos)

app.use('/filtrarHombres', routerFiltrarHombres)

app.use('/filtrarMujeres', routerFiltrarMujeres)

app.use('/buscarContacto', routerBuscarContacto)


app.use('/ordenarPorEdad', routerOrdenarPorEdad)

app.use('/todos', routerGetTodos)

app.use('/usuarios/nombreUsuario', routerNombreUsuario)

//ruta para registrar usuario
app.use('/usuarios/registrarUsuario', routerRegistrarUsuario)

//ruta para el login de usuario
app.use('/usuarios/loginUsuario', routerLoginUsuario)

//ruta para saber que tipo de alerta se debe mostrar
app.use('/tipoDeAlert', routerTipoDeAlert)

//ruta para el logout de usuario
app.use('/usuarios/logout', routerLogout)

//ruta para saber si el usuario esta autenticado
app.use('/usuarios/siEstaAutenticado', routerSiEstaAutenticado)

//ruta para que si el usuario se redirige a las rutas de login y registrar y esta autenticado, no se permita que lo haga
app.use('/usuarios/proteccionVistasLogin', routerProteccionVistasLogin)

try {
     db.authenticate();
    console.log('Conexión exitosa a la DB')
} catch (error) {
    console.log(`El error de conexión es: ${error}`)
}

 app.get('/', (req, res)=>{
    res.send('HOLA MUNDO')
}) 

app.listen(8080, ()=>{
    console.log('Server UP running in http://localhost:8080/')
})

export default app
  