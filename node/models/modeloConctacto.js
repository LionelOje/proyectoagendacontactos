import db from "../dtabase/conexionDtabase.js";
import { DataTypes } from "sequelize";

///Configuracion del modelo del contacto

 const ModeloContacto = db.define('contactos', {
     Nombre: { type: DataTypes.STRING },
     Numero: { type: DataTypes.NUMBER },
     Sexo: { type: DataTypes.STRING },
     Edad: { type: DataTypes.NUMBER },
     usuario_id: { type: DataTypes.NUMBER}
 })

 export default ModeloContacto;
