import db from "../dtabase/conexionDtabase.js";
import { DataTypes } from "sequelize";

///Configuracion del modelo del contacto

 const ModeloUsuario = db.define('usuarios', {
     Usuario: { type: DataTypes.STRING },
     Nombre: { type: DataTypes.STRING },
     Password: { type: DataTypes.STRING },
 })

 export default ModeloUsuario;
