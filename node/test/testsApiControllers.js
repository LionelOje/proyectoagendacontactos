import chai from 'chai';
import chaiHttp from 'chai-http'
import app from '../app.js'

chai.should()
chai.use(chaiHttp)


describe("GET contactos", () => {
    
    it("Metodo get que devuelve todos los contactos", (done) => {
        chai.request(app)
            .get("/contactos")
            .end((err, response) => {
                response.should.have.status(200);
                response.body.should.be.a('object');
            done();
            });
    });
    
    it("Metodo get que devuelve un contacto segun el id dado", (done) => {
        chai.request(app)
            .get("/contactos/32")
            .end((err, response) => {
                response.should.have.status(200);
                response.body.should.be.a('object');
                response.body.should.have.property('id');
                response.body.should.have.property('Nombre');
                response.body.should.have.property('Numero');
                response.body.should.have.property('id').eq(32);
            done();
            });
    });

    it("Tiene que fallar por que se le pasa un contacto con un id inexistente, entonces no devuelve nada y la propieda Nombre no existe", (done) => {
        chai.request(app)
            .get("/contactos/323")
            .end((err, response) => {
                response.should.have.status(200);
                response.body.should.have.property('Nombre');
            done();
            });
    });
})


describe("POST(crear) contacto y usuario", () => {
    it("Metodo post para crear un usuario", (done) => {
        const task = {
            Usuario:"",
            Nombre : "mengano",
            Password:"1"
        };
        chai.request(app)                
            .post("/usuarios/registrarUsuario")
            .send(task)
            .end((err, response) => {
                response.should.have.status(200);
                response.body.should.be.a('object');
                response.body.should.have.property('Nombre').eq("mengano");
            done();
            });
    });

    it("Metodo post para crear un contacto", (done) => {
        const task = {
            Nombre:"pepa",
            Numero : 12,
            Sexo: "hombre",
            Edad: 44
        };
        chai.request(app)                
            .post("/contactos")
            .send(task)
            .end((err, response) => {
                console.log(response.body)
                response.should.have.status(200);
                response.body.should.be.a('object');
            done();
            });
    });
});    



describe("PUT contactos", () => {
    it("Metodo put para editar un contacto segun el id de contacto dado", (done) => {
        const task = {
            Nombre:"pepito",
            Numero : 12,
            Sexo: "hombre",
            Edad: 44
        };
        chai.request(app)                
            .put("/contactos/31")
            .send(task)
            .end((err, response) => {
                response.should.have.status(200);
                response.body.should.be.a('object');
                response.body.should.have.property('Nombre').eq("pepito");
                response.body.should.have.property('Numero').eq(12);
            done();
            });
    });
});    


describe("DELETE contacto", () => {
    it("Metodo para eliminar un contacto de la lista segun un id dado", (done) => {

        chai.request(app)                
            .delete("/contactos/28")
            .end((err, response) => {
                response.should.have.status(200);
                response.body.should.have.property('message').eq("Contacto eliminado correctamente");
            done();
            });
    });
});
