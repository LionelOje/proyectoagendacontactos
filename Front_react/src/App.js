import React, {useEffect} from 'react';
import './GlobalStyles.css';
import VistaContactos from './Contactos/VistaContactos.js';
import VistaIngresar from './InicioSesion/VistaIngresar.js';
import VistaRegistrar from './InicioSesion/VistaRegistrar';
import VistaCrearContacto from './Contactos/VistaCrearContacto.js';
import VistaEditarContacto from './Contactos/VistaEditarContacto.js';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import axios from 'axios'
import swal from 'sweetalert'

// Rutas para consumir con axios
const URL2 = 'http://localhost:8080/usuarios/proteccionVistasLogin/'
const URL1 = 'http://localhost:8080/usuarios/siEstaAutenticado';


const App = () => {
  
  axios.defaults.withCredentials = true;
  

  // Coloca el fondo en negro 
  useEffect(() => {
    
    let backgroundBody= document.body;
    backgroundBody.style.background = 'rgb(27,27,27)';    
  },[])


  // Funcion auxiliar para usar las alertas de sweetAlert 
  const alertas = (titulo, text, icono, boton) => {
    swal({
      title: titulo,
      text: text,
      icon: icono,
      button: boton
    })
  }


  // Se consume la ruta http://localhost:8080/usuarios/proteccionVistasLogin/ para comprobar si el usuario esta logeado y en caso de que lo este lo retorna a la ruta raiz.  Se pasa como parametro lo que va a ser el navigate de react router
  const proteccionVistasLogin = async (ruta) => {
    const resp = await axios.get(URL2);
    if (resp.data.message=='esta'){
      ruta('/')
      alertas('Por aca no papu', 'Debes deslogearte para ingresar a esa direccion', 'error', 'Aceptar')    
    }  
  }


  // Se consume la ruta http://localhost:8080/usuarios/siEstaAutenticado/ para comprobar si el usuario esta logeado y en caso de que NO lo este lo retorna a la ruta para iniciar sesion. Se pasa como parametro lo que va a ser el navigate de react router
  const siEstaAutenticado = async (ruta) => {
    const resp = await axios.get(URL1);
    if (!resp.data.message){
      ruta('/ingresar')
      alertas('Por aca no papu', 'Debes iniciar sesion para ingresar a esa direccion', 'error', 'Aceptar')           
    }  
  }

  
  return (
    <>     
      <BrowserRouter>
        <Routes>
            <Route path='/' element={ <VistaContactos siEstaAutenticado={siEstaAutenticado} alertas={alertas} />} />
            <Route path='/create' element={ <VistaCrearContacto siEstaAutenticado={siEstaAutenticado} />} />
            <Route path='/edit/:id' element={ <VistaEditarContacto siEstaAutenticado={siEstaAutenticado}/>} />
            <Route path='/ingresar' element={ <VistaIngresar proteccionVistasLogin={proteccionVistasLogin} alertas={alertas}/>} />
            <Route path='/registrar' element={ <VistaRegistrar proteccionVistasLogin={proteccionVistasLogin}/>} />              
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;




// ARREGLAR LOS NOMBRES DE LOS ARCHIVOS, FIJARME EL ESPACIADO DEL CODIGO lo de los puntos y coma y los if ponerlos modernos, lo del boton cancelar ver si lo hago con un navigate para la redirecion y que de paso me vacie los imputs, de modificar lo de que aparecern numeros en los imputs hacer que no aparesca nada.modificar la ruta raiz, poner los puntos y coma, ver si puedo sacar el let passhash, centrar los formularios en direccion abajo arriba, hacer el responsive, buscar una imagen para los contactos hombre, hacer lo de la relacion, buscar una imagen de welcome el iniciar sesion y modificar los imput a como esta hecho en el proyecto del vago, ver si ese promisify es necesario y poner las variables globales env en la conexion de la base de datos, comentar algunas cosas del codigo