import React from 'react';
import './VistaIngresar.css';
import {Link} from 'react-router-dom';
import axios from 'axios'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'

// Ruta para ser consumida con axios
const URL = 'http://localhost:8080/usuarios/loginUsuario/'

const VistaIngresar = ({proteccionVistasLogin, alertas}) => {

    // useNavigate de react router    
    const navigate = useNavigate()    


    // Cuando se renderiza el componente verifica que el usuario no este logeado, en caso de estarlo, lo retorna a la ruta raiz
    useEffect( ()=>{
        proteccionVistasLogin(navigate)
    },[])


    // Estado para guardar el Usuario y Password del usuario que quiere ingresar
    const [Usuario, setUsuario] = useState('')
    const [Password, setPassword] = useState('')


    axios.defaults.withCredentials = true;


    // Procedimiento para el logeo del usuario. En caso de aver errores de validacion lanza distintas alertas. Si no hay errores de validacion, me dirije a la ruta raiz
    const loginUsuario = async (e) => {
        e.preventDefault() 
        let resp = await axios.post(URL, {Usuario: Usuario, Password: Password},{withCredentials:true})  // manda este json al back para que lo guarde en la base de datos 
        
        if (resp.data.error=='campos vacios'){
            alertas('Error','Campos vacios', 'error', 'Aceptar')
        }

        if (resp.data.error=='Usuario no existe'){
            alertas('Error','El usuario que ingreso no existe', 'error', 'Aceptar')
        }
        if (resp.data.error=='contraseña incorrecta'){
            alertas('Error','La contraseña ingresada es incorrecta', 'error', 'Aceptar')
        }

        if (!resp.data.error){
            navigate("/")
        }
    }        

return (
      <>
        <div className='container mx-auto '>
        <div className='col-8 col-sm-6 mx-auto formulario-ingresar '>
            <div className="row">
                <div >
                    <h3 className="titulo">Iniciar sesion</h3>
                </div>
            </div>
            <div className="row">
                <div className="col-10 mx-auto mt-3">
                    <form onSubmit={loginUsuario}>
                        <div className='mb-3'>
                            <label className='form-label text-light'><b>Usuario:</b></label>
                            <input
                                value={Usuario}
                                onChange={ (e)=> setUsuario(e.target.value)} 
                                type="text"
                                className='form-control'
                                placeholder="Ingrese su usuario"
                            />
                        </div>
                        <div className='mb-3'>
                            <label className='form-label text-light'><b>Password:</b></label>
                            <input
                                value={Password}
                                onChange={ (e)=> setPassword(e.target.value)}  
                                type="password"
                                className='form-control'
                                placeholder='Ingrese su contraseña'
                            />
                        </div>
                        <div className="posicion-boton">
                            <button type='submit' className='btn btn-primary col-12'>Ingresar</button>
                        </div>                 
                        <div className="posicion-boton">
                            <Link to='/registrar' className='btn btn-danger col-12'>Registrarse</Link>                  
                        </div>
                    </form>
                </div>
            </div>            
            </div>
        </div>       
     </>
    );
  }
  
  export default VistaIngresar;    