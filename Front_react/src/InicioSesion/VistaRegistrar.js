import React from 'react';
import './VistaIngresar.css';
import axios from 'axios'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import {Link} from 'react-router-dom';
import {validacionesCamposVaciosUsuarios, validacionCampoVacio} from '../Contactos/FuncionesValidaciones.js';

// Ruta para ser consumida con axios
const URL = 'http://localhost:8080/usuarios/registrarUsuario/'


const VistaRegistrar = ({proteccionVistasLogin}) => {

    axios.defaults.withCredentials = true;


    // Cuando se renderiza el componente verifica que el usuario no este logeado, en caso de que lo este me retorna a la ruta raiz
    useEffect( ()=>{
        proteccionVistasLogin(navigate)
      },[])
    

    // Funcion para comprobar que los campos que rellena el usuario esten validados.
    const estadoValidaciones = () => {
      let validacionOk = true
      
      // Si hay algun campo vacio la funcion devuelve falso, osea que no pasa la validacion
      if(!validacionesCamposVaciosUsuarios(validacionCampoVacio(Nombre), validacionCampoVacio(Usuario), validacionCampoVacio(Password))){
        return  validacionOk = false
      }
      return validacionOk
    }


    // Estados para guardar el Usuario, el nombre y el password del usuario
    const [Usuario, setUsuario] = useState('')
    const [Nombre, setNombre] = useState('')
    const [Password, setPassword] = useState('')
    
    // useNavigate de react router
    const navigate = useNavigate()    
    
    
    // Si pasa la validacion me registra un nuevo usuario y me redirije a la vista ingresar
    const registrarUsuario = async (e) => {
        e.preventDefault() 
        if (estadoValidaciones()){
          await axios.post(URL, {Usuario: Usuario, Nombre: Nombre, Password: Password})  // manda este json al back para que lo guarde en la base de datos 
          navigate('/ingresar') //luego de mandar el json me devuelve a la vista ingresar
        }    
    }

    return (
        <>
            <div className="container mt-4">
                <div className="row">
                    <div className="col-lg-8 mx-auto p-3 mb-5">

                        <div className="card border-primary">
                            <div className="card-header bg-primary text-white">Registrarse</div>
                        <div className="card-body text-white fondo">
                            <form onSubmit={registrarUsuario}>        
                                <div className="mb-3">
                                    <label  className="form-label">Nombre completo</label>
                                    <input  value={Nombre}
                                onChange={ (e)=> setNombre(e.target.value)} 
                                type="text" className="form-control" aria-describedby="emailHelp"/>              
                                </div>
                                <div className="mb-3">
                                    <label  className="form-label">Cuenta de usuario</label>
                                    <input  value={Usuario}
                                onChange={ (e)=> setUsuario(e.target.value)} 
                                type="text" className="form-control"/>
                                </div>
                                <div className="mb-3">
                                    <label  className="form-label">Password</label>
                                    <input  value={Password}
                                onChange={ (e)=> setPassword(e.target.value)} 
                                type="password" className="form-control"/>
                                </div>                            
            
            <div className="card-footer bg-transparent border-white">
              <Link to="/ingresar"  type="button" className="btn btn-secondary">Cancelar</Link>
              <button type="submit" className="btn btn-primary">Registrar</button>
            </div>
            </form>
          </div>   
        </div>
      </div>
    </div>
  </div>
</>
);
}

export default VistaRegistrar;