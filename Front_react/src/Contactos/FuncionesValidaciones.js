import swal from 'sweetalert'

export const alertas = (titulo, text, icono, boton) => {
    swal({
      title: titulo,
      text: text,
      icon: icono,
      button: boton
    })
  }

export  const validacionTipoDeDato = (valor, TieneQueSerTipo) => {
    let validacionOk = true
    let valorr = valor
    let valorPasado= parseInt(valorr)
    if (TieneQueSerTipo=='number'){ 
      if (valorPasado){
        return validacionOk
     }
      else{
        return validacionOk = false
      }
    }
    else{
      if (!valorPasado){
        return validacionOk
     }
      else{
        return validacionOk = false
      }
    }
  }  

export const validacionCampoVacio = (valor) => {
    let validacionOk = true;
    if (valor==''){
      return validacionOk = false           
    }
    else{
      return validacionOk
    }   
  }  

export const validacionesSexo = (sexo, setvalor) =>{
    let validacionOk = true
    if (sexo!='hombre' && sexo!='mujer'){
      alertas('Error genero', 'el genero debe ser hombre o mujer' , 'error', 'Aceptar')
      setvalor('')
      return validacionOk = false          
    }
    else{
      return validacionOk
    }   

  }

export const validacionesCamposVacios = (campovacio1,campovacio2,campovacio3,campovacio4) =>{
    let validacionOk = true;
    let validacionCampoVacioNombre=campovacio1
    let validacionCampoVacioNumero=campovacio2
    let validacionCampoVacioSexo=campovacio3
    let validacionCampoVacioEdad=campovacio4
  
    if (validacionCampoVacioNombre==false || validacionCampoVacioNumero==false || validacionCampoVacioEdad==false || validacionCampoVacioSexo==false){
      alertas('Campos Vacios', 'No debe haber campos vacios' , 'error', 'Aceptar')
      return validacionOk = false
    }
    else{
      return validacionOk 
    }
  }

export  const validacionesTipoDato= (tipoDatoNombre, tipoDatoNumero, tipoDatoSexo, tipoDatoEdad, setnombre, setedad, setnumero, setsexo) => {
    let validacionOk = true

    if(!tipoDatoNombre){
      alertas('Error tipo de valor', 'El nombre debe ser un valor de tipo string' , 'error', 'Aceptar')
      setnombre('')
      return validacionOk = false
    }
    
    if(!tipoDatoNumero){
      alertas('Error tipo de valor', 'El numero debe ser un valor de tipo numerico' , 'error', 'Aceptar')
      setnumero('')
      return validacionOk = false
    }

    if(!tipoDatoSexo){
      setsexo('')
      alertas('Error tipo de valor', 'El sexo debe ser un valor de tipo string' , 'error', 'Aceptar')
      return validacionOk = false
    }

    if(!tipoDatoEdad){
      setedad('')
      alertas('Error tipo de valor', 'La edad debe ser un valor de tipo numerico' , 'error', 'Aceptar')
      return validacionOk = false
    }
    return validacionOk
  }
 


  export const validacionesCamposVaciosUsuarios = (campovacio1,campovacio2,campovacio3,campovacio4) =>{
    let validacionOk = true;
    let validacionCampoVacioNombre=campovacio1
    let validacionCampoVacioUsuario=campovacio2
    let validacionCampoVacioPassword=campovacio3
  
    if (validacionCampoVacioNombre==false || validacionCampoVacioUsuario==false || validacionCampoVacioPassword==false){
      alertas('Campos Vacios', 'No debe haber campos vacios' , 'error', 'Aceptar')
      return validacionOk = false
    }
    else{
      return validacionOk 
    }
  }


