import React from 'react';
import './BuscadorContactos.css';
import axios from 'axios'
import { useState} from 'react'


const URL = 'http://localhost:8080/buscarContacto'

const BuscadorContactos = ({setcontactos, alertas}) =>{

    axios.defaults.withCredentials = true;

    const [NombreContacto, setNombreContacto] = useState('')

    const buscarContacto = async (e) => {
        e.preventDefault()

        const res = await axios.post(URL, {Nombre: NombreContacto});
        console.log(res)
        if(res.data.error==='campo vacio'){
            alertas('Error','Campo vacio', 'error', 'Aceptar')
        }
        else 
            if (res.data.error==='contacto no existe'){
                alertas('Error', 'El contacto ingresado no existe', 'error', 'Aceptar')
            }
            else{
                setcontactos(res.data);
            }  
    }

    return (
        <>
                <div className="col-8 mx-auto mt-3" >
                    <form onSubmit={buscarContacto} className="d-flex ">
                        <div className='mb-3'>
                            <input
                                value={NombreContacto}
                                onChange={ (e)=> setNombreContacto(e.target.value)} 
                                type="text"
                                className='form-control'
                                placeholder='Buscar contacto'
                            />
                        </div>
                        <button className="btn  color-btn border-radius-btn " >
                            <img className="search-icon" type="submit" src="./search-icon.png"></img>
                        </button>
                    </form>
                </div>
        </>
      );
    }
    
    export default BuscadorContactos;
  
