import React from 'react';
import Contacto from './Contacto';
import HeaderVistaContactos from './HeaderVistaContactos';
import axios from 'axios';
import {useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom'

// Rutas para consumir con axios
const URI = 'http://localhost:8080/contactos/';
const URL2 = 'http://localhost:8080/usuarios/nombreUsuario';


const VistaContactos = ({siEstaAutenticado, alertas}) => {

  // UseNavigate de react router 
  const navigate = useNavigate() 

  // Estado que contiene los contactos 
  const [contactos, setcontactos] = useState([])

  axios.defaults.withCredentials = true;


  // Funcion utilizada para que primero revise si el usuario esta logeado y una ves confirmado me renderise los contactos y coloque en la esquina el nombre del usuario activo. 
  const  authYgetcontactos =  () =>{ 
    siEstaAutenticado(navigate)
    getContactos();
    nombreUsuarioo(); 
  }

  // Ejecuta la funcion anterior cuando se renderisa el componente
  useEffect( ()=>{
    authYgetcontactos()
  },[])


  // Procedimineto para mostrar todos los contactos
  const getContactos = async () => {
      const res = await axios.get(URI);
      setcontactos(res.data);
  }


  // Estado para guardar el nombre del usuario activo
  const [nombreUsuario, setnombreUsuario] = useState(null)


  // Procedimiento para conseguir el nombre del usuario activo
  const nombreUsuarioo = async () => {
    const resp = await axios.get(URL2);
    console.log(resp.data)
    setnombreUsuario(resp.data);
  }


  let imagenAvatar;
  
  // Funcion que determina segun el genero del contacto, la imagen de perfil que tendra
  const selectorDeAvatarContacto = (genero) => {
    if (genero==="hombre"){
      imagenAvatar = 'icono-hombre.jpg';
    }
    else{
      imagenAvatar = 'icono-mujer.png';
    }
    return imagenAvatar;
  }

  


    return (
      <>
        <HeaderVistaContactos setcontactos={setcontactos} alertas={alertas} nombreUsuario={nombreUsuario}/> 
        <div className="container">
            <div className=" row ">
              <div className="col-12 flexbo mt-4"> 
              { 
               contactos.map((contacto) => <Contacto key={contacto.id} nombre={contacto.Nombre} numero={contacto.Numero} genero={contacto.Sexo} edad={contacto.Edad}  contactoId={contacto.id} imagenAvatar={selectorDeAvatarContacto(contacto.Sexo)} getContactos={getContactos} />)
              }
              </div>
            </div>
          </div>
      </>
    );
  }
  
  export default VistaContactos;
