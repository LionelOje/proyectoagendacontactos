import React from 'react';
import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate, useParams, Link} from "react-router-dom";
import {validacionesTipoDato, validacionTipoDeDato, validacionesCamposVacios, validacionCampoVacio, validacionesSexo} from './FuncionesValidaciones';

// Ruta para ser consumida con axios
const URL = 'http://localhost:8080/contactos/'


const CrearContacto = ({siEstaAutenticado}) => {

    // Estados para guardar el Nombre Numero Sexo y Edad del contacto
    const [Nombre, setNombre] = useState('')
    const [Numero, setNumero] = useState('')
    const [Sexo, setSexo] = useState('')
    const [Edad, setEdad] = useState('')

    // UseNavigate de react router
    const navigate = useNavigate()    


    // Esto es para obtener el id del contacto a editar. Esto del useparams lo uso cuando quiero pasar parametros a otras vistas y se pasan por medio de las rutas, ejemplo esta ruta es /edit/:id y ese id lo paso a traves del <link to="edit/:id" que es el boton que me accede a esta vista. Entonces si tengo que pasar parametros en el arbol de compoennte s de una misma vista lo hago de la forma normal pero si es una vista con otra ruta lo hago por medio de useparams
    const {id} = useParams()


    // Funcion para comprobar que los campos que rellena el usuario esten validados.    
    const estadoValidaciones = () => {
        let validacionOk = true
        let sexoo = Sexo
        sexoo.toLowerCase()
    
        // Si hay algun campo vacio la funcion devuelve falso, osea que no pasa la validacion
        if(!validacionesCamposVacios(validacionCampoVacio(Nombre), validacionCampoVacio(Numero), validacionCampoVacio(Sexo), validacionCampoVacio(Edad))){
          return  validacionOk = false
        }

        // Si hay algun error en el tipo de dato, ej en ves de un numero coloco un string, la funcion devuelve falso, osea que no pasa la validacion. Ademas me manda un alerta con el error y me vacia el campo mal colocado.
        if(!validacionesTipoDato(validacionTipoDeDato(Nombre, 'string'), validacionTipoDeDato(Numero, 'number'), validacionTipoDeDato(Sexo, 'string'), validacionTipoDeDato(Edad, 'number'), setNombre, setEdad, setNumero, setSexo)){
          return  validacionOk = false
        }

        // Si el sexo no es hombre o mujer, me devuelve falso, osea que no pasa la validacion. Ademas me manda un alerta con el error y me vacia el campo mal colocado
        if(!validacionesSexo(sexoo, setSexo)){
          return  validacionOk = false
        }
        return validacionOk
    }


    //Si pasa la validacion utiliza el id del contacto al que se lo quiere editar y lo edita. Luego retorna a la ruta raiz
    const update = async (e) => {
        e.preventDefault()
        if (estadoValidaciones()){
            await axios.put(URI+id, {Nombre: Nombre, Numero: Numero, Sexo: Sexo, Edad: Edad})
            navigate('/')
        }
    }


    //Esto lo hago para que obtener el contacto al cual se quiere editar. Lo consigo por medio del id y los datos de nombre numero etc los uso para que me aparescan en el imput en donde se escriben los nuevos datos
    const getBlogById = async () => {
        const res = await axios.get(URL+id)
        setNombre(res.data.Nombre)
        setNumero(res.data.Numero)
        setSexo(res.data.Sexo)
        setEdad(res.data.Edad)
    }


    // Apenas se renderisa el componente se ejecuta la funcion para obtener los datos del contacto cliqueado. Tambien se comprueba si el usuerio esta logeado, si no lo esta lo manda a la vista de iniciar sesion
    useEffect( ()=>{
        siEstaAutenticado(navigate)
        getBlogById()
    },[])
    
    return (
      <>
        <div className='container mt-4'>
            <div className=" row ">
                <div className="col-12 text-center text-light">
                    <h3>Editar CONTACTO</h3>
                </div>
            </div>
            <div className=" row ">
                <div className="col-6 mx-auto mt-4">
                    <form onSubmit={update}>
                        <div className='mb-3'>
                            <label className='form-label text-light'><b>Nombre:</b></label>
                            <input
                                value={Nombre}
                                onChange={ (e)=> setNombre(e.target.value)} 
                                type="text"
                                className='form-control'
                            />
                        </div>
                        <div className='mb-3'>
                            <label className='form-label text-light'><b>Numero:</b></label>
                            <input
                                value={Numero}
                                onChange={ (e)=> setNumero(e.target.value)}  
                                type="text"
                                className='form-control'
                            />
                        </div>
                        <div className='mb-3'>
                            <label className='form-label text-light'><b>Genero:</b></label>
                            <input
                                value={Sexo}
                                onChange={ (e)=> setSexo(e.target.value)}  
                                type="text"
                                className='form-control'
                            />
                        </div>
                        <div className='mb-3'>
                            <label className='form-label text-light'><b>Edad:</b></label>
                            <input
                                value={Edad}
                                onChange={ (e)=> setEdad(e.target.value)}  
                                type="text"
                                className='form-control'
                            />
                        </div>
                        <div className=" bg-transparent border-white posicion-boton">
                            <button type="submit" className="btn btn-primary me-1">Guardar contacto</button>
                            <Link to="/"  type="button" className="btn btn-danger">Cancelar</Link>
                        </div>
                    </form>
                </div>
            </div>            
        </div>       
      </>
    );
  }
  
  export default CrearContacto;
