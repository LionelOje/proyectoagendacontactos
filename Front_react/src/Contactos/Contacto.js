import React from 'react';
import './Contacto.css';
import {Link} from 'react-router-dom';
import axios from 'axios';

const URI = 'http://localhost:8080/contactos/';


const Contacto = ({nombre, numero, genero, edad, imagenAvatar, getContactos, contactoId}) => {

  //procedimineto para eliminar un blog
  const deleteContacto = async (id) => {
    await axios.delete(`${URI}${id}`);
    getContactos();
  }

  return (
      <>
            <div className="col-5 col-sm-5 col-md-4 col-lg-3 tarjeta-contacto"> 
                <img src={imagenAvatar} className="img-contacto col-7"></img> 
                <p><b className="me-1">Nombre:</b>{nombre}.</p>
                <p><b className="me-1">Telefono:</b></p>
                <p>{numero}</p>
                <p><b className="me-1">Genero:</b>{genero}.</p>
                <p><b className="me-1">Edad:</b>{edad}.</p>
                <div className="posicion-iconos">
                  <Link to={`/edit/${contactoId}`} className="btn btn-primary"><i className="iconos fa-solid fa-pen-to-square"></i> </Link>
                  <button onClick={()=> {deleteContacto(contactoId)}} className="btn btn-danger"><i className="iconos fa-solid fa-trash-can"></i></button>
                </div>
            </div>
      </>
    );
  }
  
  export default Contacto;