import React from 'react';
import './CrearEditarContacto.css';
import axios from 'axios'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import {Link} from 'react-router-dom';
import {validacionesTipoDato, validacionTipoDeDato, validacionesCamposVacios, validacionCampoVacio, validacionesSexo} from './FuncionesValidaciones';

// Ruta para ser consumida con axios
const URL = 'http://localhost:8080/contactos/'


const CrearContacto = ({siEstaAutenticado}) => {

    axios.defaults.withCredentials = true;


    // Comprueba si el usuario esta logeado al renderisar el componente
    useEffect( ()=>{
        siEstaAutenticado(navigate);
      },[])

    
    // Estados para guardar el Nombre Numero Sexo y Edad del contacto  
    const [Nombre, setNombre] = useState('')
    const [Numero, setNumero] = useState('')
    const [Sexo, setSexo] = useState('')
    const [Edad, setEdad] = useState('')

    // UseNavigate de react router
    const navigate = useNavigate()    
    

    // Funcion para comprobar que los campos que rellena el usuario esten validados.
    const estadoValidaciones = () => {
    let validacionOk = true
    let sexoo = Sexo
    sexoo.toLowerCase()

    // Si hay algun campo vacio la funcion devuelve falso, osea que no pasa la validacion
    if(!validacionesCamposVacios(validacionCampoVacio(Nombre), validacionCampoVacio(Numero), validacionCampoVacio(Sexo), validacionCampoVacio(Edad))){
      return  validacionOk = false
    }

    // Si hay algun error en el tipo de dato, ej en ves de un numero coloco un string, la funcion devuelve falso, osea que no pasa la validacion. Ademas me manda un alerta con el error y me vacia el campo mal colocado.
    if(!validacionesTipoDato(validacionTipoDeDato(Nombre, 'string'), validacionTipoDeDato(Numero, 'number'), validacionTipoDeDato(Sexo, 'string'), validacionTipoDeDato(Edad, 'number'), setNombre, setEdad, setNumero, setSexo)){
      return  validacionOk = false
    }

    // Si el sexo no es hombre o mujer, me devuelve falso, osea que no pasa la validacion. Ademas me manda un alerta con el error y me vacia el campo mal colocado
    if(!validacionesSexo(sexoo, setSexo)){
      return  validacionOk = false
    }
    return validacionOk
  }

    
    // Si los campos que rellena el usuario estan validados, me crea un nuevo contacto en la base de datos
    const store = async (e) => {
        e.preventDefault()
     
        if(estadoValidaciones()){
            await axios.post(URL, {Nombre: Nombre, Numero: Numero, Sexo: Sexo, Edad: Edad})  // manda este json al back para que lo guarde en la base de datos 
        navigate('/') //luego de mandar el json me devuelve a la vista vistaContactos
        }
    }    

    return (
      <>
        <div className='container mt-1'>
            <div className=" row ">
                <div className="col-12 text-center text-light">
                    <h3>Agregar CONTACTO</h3>
                </div>
            </div>
            <div className=" row ">
                <div className="col-5 mx-auto mt-4">
                    <form onSubmit={store}>
                        <div className='mb-3'>
                            <label className='form-label text-light'><b>Nombre:</b></label>
                            <input
                                value={Nombre}
                                onChange={ (e)=> setNombre(e.target.value)} 
                                type="text"
                                className='form-control'
                            />
                        </div>
                        <div className='mb-3'>
                            <label className='form-label text-light'><b>Numero:</b></label>
                            <input
                                value={Numero}
                                onChange={ (e)=> setNumero(e.target.value)}  
                                type="text"
                                className='form-control'
                            />
                        </div>
                        <div className='mb-3'>
                            <label className='form-label text-light'><b>Genero:</b></label>
                            <input
                                value={Sexo}
                                onChange={ (e)=> setSexo(e.target.value)}  
                                type="text"
                                className='form-control'
                            />
                        </div>
                        <div className='mb-3'>
                            <label className='form-label text-light'><b>Edad:</b></label>
                            <input
                                value={Edad}
                                onChange={ (e)=> setEdad(e.target.value)}  
                                type="text"
                                className='form-control'
                            />
                        </div>
                        <div className=" bg-transparent border-white posicion-boton">
                            <button type="submit" className="btn btn-primary me-1">Guardar contacto</button>
                            <Link to="/"  type="button" className="btn btn-danger">Cancelar</Link>
                        </div>
                    </form>
                </div>
            </div>            
        </div>       
      </>
    );
  }
  
  export default CrearContacto;

