import Dropdown from 'react-bootstrap/Dropdown';
import {useState} from 'react';
import React from 'react';
import './HeaderVistaContactos.css';
import {Link} from 'react-router-dom';
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import BuscadorContactos from './BuscadorContactos';

//col-sm-6 col-md-5

const URL1 = 'http://localhost:8080/usuarios/logout';
const URL2 = 'http://localhost:8080/ordenarPorEdad';
const URL3 = 'http://localhost:8080/filtrarHombres';
const URL4 = 'http://localhost:8080/filtrarMujeres';
 

const HeaderVistaContactos = ({setcontactos, nombreUsuario, alertas}) => {

  const navigate = useNavigate()    

  const logout = async () => {
    const res = await axios.get(URL1);
    if(res.data.message){
      navigate("/ingresar")
    }
  }
  
  const ordenarPorEdad = async () => {
      const res = await axios.get(URL2);
      setcontactos(res.data);
    
  }

  const filtrarHombres = async () => {
      const res = await axios.get(URL3);
      setcontactos(res.data);
    
  }

  const filtrarMujeres = async () => {
      const res = await axios.get(URL4);
      setcontactos(res.data);
    
  }
  
    return (
      <>
        <div className="container ">
          <div className=" row ">
            <div className="col-12 d-flex "> 
                <div className="col-4  d-flex ">
                  <div className="col-4  mt-3 ">
                  <Dropdown>
                    <Dropdown.Toggle className="col-8 col-lg-10" variant="outline-primary" id="dropdown-basic">
                      Filtros
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      <Dropdown.Item onClick={() => ordenarPorEdad()} href="#/action-1">Ordenar por edad</Dropdown.Item>
                      <Dropdown.Item onClick={() => filtrarHombres()} href="#/action-2">Filtrar hombres</Dropdown.Item>
                      <Dropdown.Item onClick={() => filtrarMujeres()} href="#/action-3">Filtrar mujeres</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                  </div>
                  <div className="col-4   mt-3">
                  <Link  to='/create' className="col-8 col-lg-10 btn btn-outline-secondary">Crear contacto</Link>
                  </div>
                  <div className="col-4  mt-3">
                  <button onClick={()=> {logout()} } className="col-8 col-lg-10 btn btn-outline-danger">Log out</button>
                  </div>  
                </div>
                <div className="col-4">
                  <BuscadorContactos setcontactos={setcontactos} alertas={alertas}/>
                </div>
                <div className="col-4 mt-2 texto-bienvenida">
                  <p >Bienbenido: {nombreUsuario}</p>
                </div>
            </div>
          </div>                                                                
       </div>
      </>
    );
  }
  
  export default HeaderVistaContactos;